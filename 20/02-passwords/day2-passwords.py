# Advent of Code 2020, Day 2
# https://adventofcode.com/2020/day/2#
count = 0
with open ('input.txt', 'r') as f:
    for line in f:
        minmax, char, pw = line.split()
        min = int(minmax.split(sep='-')[0])
        max = int(minmax.split(sep='-')[1])
        charCount = pw.count(char[0])
        if charCount >= min and charCount <= max:
            count += 1

print ('There are {} valid passwords'.format(count))

