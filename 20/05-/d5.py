

with open('input.txt', 'r') as f:
    max = 0
    for line in f:
        value = 64
        row = 0
        for i in range(8):
            if line[i] == "B":
                row += value
            value /= 2
        value = 4
        col = 0
        for i in range (7, 10):
            if line[i] == "R":
                col += value
            value /= 2
        id = row * 8 + col
        if id > max:
            max = id
        print (line, row, col, id)

print ('highest ID:', max)
