# Advent of code day 1.1 solution (From 2020):  
# https://adventofcode.com/2020/day/1#
ex = []
with open ('input2.txt', 'r', encoding = "ascii") as file:
    while True:
        try:
            ex.append(int (file.readline()))
        except: 
            break
    print (ex)
i= 0
j = 0

for i in range (len(ex)):
    for j in range(i, len(ex)):
        for k in range(j, len (ex)): 
            if ex[i] + ex[j] + ex[k]== 2020:
                a = i
                b = j
                c = k
                break
        
print (" index {} ({}) + {} ({}) + {} ({}) = 2020. Product is {}".format(a, ex[a], b, ex[b], c, ex[c], ex[a]*ex[b]*ex[c]))