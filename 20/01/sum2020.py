# Advent of code day 1.1 solution (From 2020):  
# https://adventofcode.com/2020/day/1#
ex = []
with open ('input2.txt', 'r') as file:
    for line in file:
            ex.append(int(line))
print (ex)
i = 0
j = 0

for i in range (len(ex)):
    for j in range(i, len(ex)):
        if ex[i] + ex[j] == 2020:
            a = i
            b = j
            break
        
print (" index {} ({}) + {} ({}) = 2020. Product is {}".format(a, ex[a], b, ex[b], ex[a]*ex[b]))