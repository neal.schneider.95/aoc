# Advent of Code 2020, Day 2
# https://adventofcode.com/2020/day/2

def count_trees(right, down) :
    row = 0
    count = 0
    with open ('.\\input.txt', 'r') as f:
        if down == 1:
            for line in f:
                index = (row*right)%(len(line)-1) 
                if line[index] == '#':
                    count += 1
                row += 1
                if row < 30: print (line[index:]+'X'+line[:index+1])
        else:
            for line in f:
                if row % down == 0:
                    index = (row*right)%(len(line)-1) 
                if line[index] == '#':
                    count += 1
                row += 1
                if row < 30: print (line[index:]+'X'+line[:index+1])
    print ('There are {} trees in the {}-{} path'.format(count, right, down))
    return count


paths = [(1,1),(3,1),(5,1),(7,1),(1,2)]
prod = 1
for right, down in paths:
    prod *= count_trees(right, down)
print ('The product is: {}'.format(prod))
