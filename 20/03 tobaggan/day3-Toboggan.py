# Advent of Code 2020, Day 2
# https://adventofcode.com/2020/day/2

def count_trees(right, down) :
    row = 0
    count = 0
    with open ('.\\input.txt', 'r') as f:
        for line in f:
            if row % down == 0:
                index = (row*3)%(len(line)-1)
                if line[index] == '#':
                    count += 1
                row += 1

    print ('There are {} trees in the {}-{} path'.format(count, right, down,))
