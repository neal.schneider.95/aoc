# AOC day 16-packets

# https://adventofcode.com/2021/

with open ('input.txt', 'r') as file:
    inputstr = file.readline()

bits = int('0xF' + inputstr, base=16)
print (bin(bits))
