# Advent of code day 3.2 solution:  
# https://adventofcode.com/2021/day/3#

bins = []

BITWIDTH = 12
with open ('input.txt', 'r') as file:
    for line in file:
        bins.append(line)

    # print (bins)
ones = [0 for i in range(BITWIDTH)]
zeros = [0 for i in range(BITWIDTH)]

for line in range(len(bins)):
    for i in range(BITWIDTH):
        if bins[line][i] == '0':
            zeros[i] += 1
        else:
            ones[i] += 1

print (ones)
print (zeros)

gamma = 0
epsilon = 0
for i in range(BITWIDTH):
    if ones[i] > zeros[i]:
        gamma = gamma * 2 + 1
        epsilon = epsilon * 2
    else:
        gamma = gamma * 2 
        epsilon = epsilon * 2 + 1

print ('Gamma = {}, Epsilon = {}, Product = {}'.format (gamma, epsilon, gamma*epsilon))

def count_pos (lyst, pos, search):
    count = 0
    for item in lyst:
        if item[pos] == search:
            count += 1
    return count

def pop_pos (lyst, pos, search):
    i = 0
    while i < len(lyst):
        if lyst[i][pos] == search:
            lyst.pop(i)
        else:
            i += 1


oxlyst = co2lyst = bins

# find ox gen rating
index = 0
while len (oxlyst) > 1:
    if count_pos(oxlyst, index, '1') > len(oxlyst)/2:
        pop_pos(oxlyst, index, '0')
    else:
        pop_pos(oxlyst, index, '1') 
    index += 1
    if index > BITWIDTH: break
    
print ('Oxygen gen:', oxlyst)

while len (co2lyst) > 1:
    if count_pos(co2lyst, index, '1') > len(oxlyst)/2:
        pop_pos(co2lyst, index, '1')
    else:
        pop_pos(co2lyst, index, '0') 
    index += 1
    if index > BITWIDTH: break
    
print ('CO2 scrub:', co2lyst)
print ('Product = ', int ('0b' + (oxlyst[0].strip())) * int ('0b' + (co2lyst[0].strip())))


        
# print ('Depth: {} Distance: {}, Product {}'.format(depth, dist, depth*dist))