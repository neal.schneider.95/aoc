# Advent of code day 1.2 solution:  
# https://adventofcode.com/2021/day/1#
depths = []
larger = 0

with open ('input.txt', 'r') as file:
    for line in file:
            depths.append(int(line))

print (depths)

# MESSY Original version
#  with open ('input.txt', 'r', encoding = "ascii") as file:
#     while True:
#         try:
#             depths.append(int (file.readline()))
#         except: 
#             break
#     # print (depths)

for i in range (1, len(depths)-2):
    # if depths[i-1] + depths[i] + depths[i+1] < depths[i] + depths[i+1] +depths[i+2]:
    if depths[i-1] < depths[i+2]:  # i and i+1 are common to both sides so can be dropped.    
        larger += 1
print (larger)