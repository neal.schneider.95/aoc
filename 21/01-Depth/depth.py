# Advent of code day 1.1 solution:  
# https://adventofcode.com/2021/day/1#
depths = []
larger = 0
with open ('input.txt', 'r') as file:
    for line in file:
            depths.append(int(line))

print (depths)

for i in range (1, len(depths)):
    if depths[i-1] < depths[i]:
        larger += 1
print (larger)