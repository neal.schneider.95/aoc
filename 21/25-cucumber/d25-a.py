# Advent of code day 25 solution:  
# https://adventofcode.com/2021/

grid = []
line_num = 0
width = 0
height = 0
dic = {'.': 0, '>': 1, 'v': 2}

def print_grid():
    for line in grid:
        for i in line:
            if i == 0: print ('.', end='')
            elif i == 1: print ('>', end='')
            else: print ('v', end='')
        print()

def move(grid):
    newgrid = [[0 for col in range(width)] for j in range(height)]
    # move the eastward cucumbers
    for row in range(height):
        for col in range(width):
            if grid[row][col] == 1:
                newgrid[row][col] = 1
                if not grid[row][(col+1) % width]:
                    if col== width - 1 and grid[row][0]:
                        continue
                    newgrid[row][(col+1) % width] = 1 
                    newgrid[row][col] = 0

    # Move southward cucumbers
    for col in range(width):
        for row in range(height):
            if grid[row][col] == 2:
                newgrid[row][col] = 2
                if not grid[(row+1)% height][col]:
                    if row == height - 1 and grid[row][0]:
                        continue
                    newgrid[(row+1) % height][col] = 2
                    newgrid[row][col] = 0
    return newgrid

def grid_value():
    val = 0
    for line in grid:
        for c in line:
            val = val*3 + c
    return val
     
if __name__ == '__main__':

    with open ('sample2.txt', 'r') as file:
        for line in file:
            grid.append([])
            for c in line[:-1]:
                grid[line_num].append(dic[c])
            line_num += 1
    width = len (grid[0])
    height = len(grid)
    print ('w', width, 'h', height)

    print_grid()
    old_grid_val = 0 
    grid_val = grid_value()
    i = 0
    # while grid_val != old_grid_val:
    for i in range (10):
        print('_______ iteration: ', i)
        grid = move(grid)
        print_grid()
        i += 1
        old_grid_val = grid_val
        grid_val = grid_value()
        