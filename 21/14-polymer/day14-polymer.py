# Advent of code day 14 solution:  
# https://adventofcode.com/2021/day/

import time

with open ('input.txt', 'r') as file:
    polymer = file.readline()
    rules_str = file.readlines()
rules = {}
for rule in rules_str:
    rules[rule[0:2]] = rule[6]
# print (rules)

def grow(polymer:str):
    new_polymer = polymer[0]
    for i in range(len(polymer) - 1):
        pair = polymer[i:i+2]
        if pair in rules.keys():
            new_polymer += rules[pair]+pair[1]
        else:
            new_polymer += pair[1]
    return new_polymer

#  Iterate through steps of p
# print (polymer)
timer = time.time()
for i in range (30):
    polymer = grow(polymer)
    # print(polymer)
    current_time = time.time()
    print ('iteration: {} time: {}'.format (i, current_time-timer))
    timer = current_time

# create dict of character to number of occurences
counts = {}
# counts.setdefault('',0)
for i in range (len(polymer)-1):
    if polymer[i] in counts.keys():
        counts[polymer[i]] += 1
    else: 
        counts[polymer[i]] = 1
print (counts)

minchar = maxchar = polymer[0]
min = max = counts[polymer[0]]
for char_count in counts.values():
    if char_count > max: max = char_count
    if char_count < min: min = char_count

print ('diff =', max-min)