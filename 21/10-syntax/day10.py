# Advent of code day 10 solution:  
# https://adventofcode.com/2021/day/3#

OPEN = '([{<'
CLOSE = ')]}>'
point_dict = {')':3, ']':57, '}':1197, '>':25137, '':0 }
point_dict2 = {'(':1, '[':2, '{':3, '<':4, '':0 }

def checkline(s:str) -> tuple: # returns tuple(invalid char, position)
    stack = []
    for i, c in enumerate(s):
        if c in OPEN:
            stack.append(c)
        elif c in CLOSE:
            if CLOSE.index(c) == OPEN.index(stack[-1]):
                stack.pop()
            else:
                return c, i
    complete = 0
    while (stack):
        complete = complete * 5 + point_dict2[stack.pop()]
    return '', complete


sum = 0
complete_scores = []

with open ('input.txt', 'r') as file:
    for line in file:
        c, i = checkline(line)
        # print (line)
        if c: 
            sum += point_dict[c]
            print (c, point_dict[c], sum)
        else:
            complete_scores.append (i)

print ('sum =', sum)

print ('part 2:')
complete_scores.sort()
print (complete_scores)
print ('Middle = ', complete_scores[int(len(complete_scores)/2)])