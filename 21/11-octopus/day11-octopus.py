# Advent of code day 11 solution:  
# https://adventofcode.com/2021/

grid = []
gridsize = 10
flashes = [0]

def read_grid():
    with open ('sample.txt', 'r') as file:
        for line in file:
            grid.append(list(int(i) for i in line[:-1]))
    # print (grid)

def print_grid():
    for row in grid:
        for item in row:
            print (item, end='')
        print()

def flash(row, col):
    # flash and bump up energies around a flashing octopus
    if row == 0:
        start_row = 0
    else:
        start_row = row - 1
    if row == gridsize:
        end_row = row
    else:
        end_row = row + 1
    if col == 0:
        start_col = 0
    else: 
        start_col = col - 1
    if col == gridsize:
        end_col = gridsize
    else: 
        end_col = col - 1
    for y in range(start_row, end_row):
        for x in (range(start_col, end_col)):
            grid[y][x] += 1
    grid[row][col] = 0
    flashes[0] += 1

def step():
    # Increment energies
    flashes = 0
    flashers = [] # tuples for coordinates of octopi that flash
    for row_num, row_data in enumerate(grid):
        for col_num, octo in enumerate (row_data):
            grid[row_num][col_num] += 1
            if grid[row_num][col_num] > 9:
                flashers.append ((row_num, col_num))
    for f in flashers:
        flash(f[0], f[1])

    # flash the ones that went over from neighbors flashing
    flashers = [] # tuples for coordinates of octopi that flash
    for row_num, row_data in enumerate(grid):
        for col_num, octo in enumerate (row_data):
            if grid[row_num][col_num] > 9:
                flashers.append ((row_num, col_num))
                flashes += 1
    for f in flashers:
        flash(f[0], f[1])

# Begin main

read_grid()
print ('Initial State:')
print_grid()
step()
print('\n step 1:')
print_grid()
step()
print('\n step 2:')
print_grid()
        
    
    
    
    



