# Advent of code day 2.1 solution:  
# https://adventofcode.com/2021/day/2#
dist = 0
depth = 0

with open ('input.txt', 'r') as file:
    for line in file:
            command, stepstr = line.split()
            step = int(stepstr)
            if command == 'forward' : dist +=  step
            elif command == 'down'  : depth += step
            elif command == 'up'    : depth -= step

print ('Depth: {} Distance: {}, Product {}'.format(depth, dist, depth*dist))
