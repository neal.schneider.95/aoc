# Advent of code day 2.2 solution:  
# https://adventofcode.com/2021/day/2#
dist = 0
depth = 0
aim = 0

with open ('input.txt', 'r') as file:
    for line in file:
            command, stepstr = line.split()
            step = int(stepstr)
            if command == 'forward' :
                dist += step
                depth += step * aim
            elif command == 'down' : aim += step
            elif command == 'up' : aim -= step

print ('Depth: {} Distance: {}, Product {}'.format(depth, dist, depth*dist))