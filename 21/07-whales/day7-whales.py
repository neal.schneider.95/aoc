# Advent of code day 7 solution:  
# https://adventofcode.com/2021/day/3#

def fuel_cost( crab_list: list, pos:int) -> int:
    sum_fuel = 0
    for crab in crab_list:
        sum_fuel += abs (crab - pos)
    return sum_fuel

def fuel2 (dist:int) -> int:
    return int(dist * (dist + 1) / 2)

def fuel_cost2( crab_list: list, pos:int) -> int:
    sum_fuel = 0
    for crab in crab_list:
        sum_fuel += fuel2(abs(crab - pos))
    return sum_fuel

crabs = []

with open ('input.txt', 'r') as file:
    crabstr = file.readline().split(',')

sum = 0
min = max = int(crabstr[0])
for s in crabstr:
    int_crab = int(s)
    crabs.append(int_crab)
    if int_crab < min: min = int_crab
    elif int_crab > max: max = int_crab

# print (crabs)

if __name__ == '__main__':

    min_fuel = fuel_cost(crabs, crabs[0])
    pos = 0
    for i in range (min, max):
        fuel = fuel_cost(crabs, i)
        if fuel < min_fuel: 
            min_fuel = fuel
            pos = i
    print ('min fuel  = {} at pos {}'.format(min_fuel, pos))

    min_fuel = fuel_cost(crabs, crabs[0])
    pos = 0
    for i in range (min, max):
        fuel = fuel_cost2(crabs, i)
        if fuel < min_fuel: 
            min_fuel = fuel
            pos = i
    print ('Part 2: \nmin fuel  = {} at pos {}'.format(min_fuel, pos))
