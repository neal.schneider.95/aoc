fish = [4,5,3,2,3,3,2,4,2,1,2,4,5,2,2,2,4,1,1,1,5,1,1,2,5,2,1,1,4,4,5,5,1,2,1,1,5,3,5,2,4,3,2,4,5,3,2,1,4,1,3,1,2,4,1,1,4,1,4,2,5,1,4,3,5,2,4,5,4,2,2,5,1,1,2,4,1,4,4,1,1,3,1,2,3,2,5,5,1,1,5,2,4,2,2,4,1,1,1,4,2,2,3,1,2,4,5,4,5,4,2,3,1,4,1,3,1,2,3,3,2,4,3,3,3,1,4,2,3,4,2,1,5,4,2,4,4,3,2,1,5,3,1,4,1,1,5,4,2,4,2,2,4,4,4,1,4,2,4,1,1,3,5,1,5,5,1,3,2,2,3,5,3,1,1,4,4,1,3,3,3,5,1,1,2,5,5,5,2,4,1,5,1,2,1,1,1,4,3,1,5,2,3,1,3,1,4,1,3,5,4,5,1,3,4,2,1,5,1,3,4,5,5,2,1,2,1,1,1,4,3,1,4,2,3,1,3,5,1,4,5,3,1,3,3,2,2,1,5,5,4,3,2,1,5,1,3,1,3,5,1,1,2,1,1,1,5,2,1,1,3,2,1,5,5,5,1,1,5,1,4,1,5,4,2,4,5,2,4,3,2,5,4,1,1,2,4,3,2,1]
#fish = [3,4,3,1,2]
# list implementation
# for i in range (80):
#     for f in range(len(fish)) :
#         fish[f] -= 1
#         if fish[f] == -1:
#             fish[f] = 6
#             fish.append(8)
# 
# print ("Fish = ", fish)
# print ("Fish Count =", len(fish))

# Solution with binning fish by age
# setup fishcount
ages = [0 for i in range(9)]
# get the ages of the fish and set up the initial state
for i in range(len(fish)):
    ages[fish[i]] += 1 
    
for day in range(256):
    print(ages)
    spawning = ages.pop(0)
    ages.append(spawning) 
    ages[6] += spawning

sum = 0
for i in range(len(ages)):
    sum += ages[i]
print ('fish = ', sum)
