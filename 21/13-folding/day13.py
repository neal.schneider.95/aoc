# Advent of code day 13 solution:  
# https://adventofcode.com/2021/

dots = [[False for x in range(655*2+1)] for y in range(447*2+1)]
with open ('input.txt', 'r') as file:
    for line in file:
        if len(line) > 1:
            cord = line.split(',')
            x = int(cord[0])
            y = int(cord[1])
        else: 
            break
        if x > 655: x = 655*2 - x
        dots[x][y] = True
    
count = 0
for x in range (655):
    for y in range (447*2 + 1):
        if dots[x][y] : count += 1

print (count)