# Advent of code day 13 solution:  
# https://adventofcode.com/2021/


dots = [[False for y in range(447*2+1)]for x in range(655*2+1)]
folds = []
with open ('input.txt', 'r') as file:
    for line in file:
        if ',' in line:
            cord = line.split(',')
            x = int(cord[0])
            y = int(cord[1])
            dots[x][y] = True
        elif 'f' in line:
            sp = line.split('=')
            folds.append(( sp[0][-1], int(sp[1])))
            
print (folds)
xsize = 655 * 2
ysize = 447 * 2 
for fold in folds:
    if fold[0] == 'x': 
        for y in range (int(ysize/2)):
            for x in range (fold[1]):
                dots[x][y] |= dots[fold[1]*2-x][y]
        xsize = int(xsize/2)
    else:
        for x in range (int(xsize/2)):
            for y in range (fold[1]):
                dots[x][y] |= dots[x][fold[1]*2-x]
        ysize = int(ysize/2)

for y in range (ysize*4):
    for x in range (xsize*4):
        if dots[x][y]: 
            print ('#', end='')
        else:
            print (' ', end='')
    print()
print ('Done')