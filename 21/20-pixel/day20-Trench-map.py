# Advent of code day 20 solution:  
# https://adventofcode.com/2021/

char_map = {'.': 0, '#': 1}

def read_data() -> list:   # returns algo and initial map state (grid)
    grid = []
    with open ('input.txt', 'r') as file:
        algo_str = file.readline()
        algo = list(char_map[i] for i in algo_str[:-1])

        line = file.readline() # discard blank line

        while line := file.readline():
            grid.append(list(char_map[col] for col in line[:-1]))

        return grid, algo

def print_grid(grid):
    bits = 0
    for row in grid:
        for item in row:
            if item: 
                print ('#', end='')
                bits += 1
            else:
                print ('.', end='')
        print()
    return bits

def get_raw_val(grid, row, col, inf):
    val = 0
    for y in range(row-1, row+2):
        for x in range (col-1, col+2):
            tmp = inf
            try: 
                tmp = grid[y][x]
            except:
                val = val*2 + inf
            else: 
                val = val*2 + tmp
    return val

def process_image(grid, algo, inf):
    # Add infinity around the grid
    rows = len(grid)
    cols = len(grid[0])
    grid.insert(0, [inf for i in range(cols)])
    for row in range(rows+1):
        grid[row].insert(0,inf)
        grid[row].append(inf)
    grid.append([inf for i in range(cols+2)])
    print_grid(grid)

    # Loop through the image
    newgrid = []
    for y in range(rows+2):
        newgrid.append([algo[get_raw_val(grid,y,x,inf)] for x in range(cols+2)])
        # newgrid.append([get_raw_val(grid,y,x) for x in range(cols+2)])
    return newgrid

# Program Start
grid, algo = read_data()
# print(algo)
# print ("Algo len:", len(algo))
print_grid(grid)

for i in range(len(grid[0])):
    print ("center raw Value ", i, '=', oct(get_raw_val(grid,i,i,0)))

grid1 = process_image(grid,algo,0)
print ('step1:')
print_grid(grid1)

grid2 = process_image(grid1, algo,1)
print ('step2:')
pix = print_grid(grid2)
print ('# Pixels: ', pix)

for i in range (50):
    grid = process_image(grid,algo,i%2)

print ('after 50:')
pix = print_grid(grid)
print ('# Pixels: ', pix)


# --- Day 20: Trench Map ---
# With the scanners fully deployed, you turn their attention to mapping the floor of the ocean trench.

# When you get back the image from the scanners, it seems to just be random noise. Perhaps you can combine an image enhancement algorithm and the input image (your puzzle input) to clean it up a little.

# For example:

# ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
# #..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
# .######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
# .#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
# .#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
# ...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
# ..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

# #..#.
# #....
# ##..#
# ..#..
# ..###
# The first section is the image enhancement algorithm. It is normally given on a single line, but it has been wrapped to multiple lines in this example for legibility. The second section is the input image, a two-dimensional grid of light pixels (#) and dark pixels (.).

# The image enhancement algorithm describes how to enhance an image by simultaneously converting all pixels in the input image into an output image. Each pixel of the output image is determined by looking at a 3x3 square of pixels centered on the corresponding input image pixel. So, to determine the value of the pixel at (5,10) in the output image, nine pixels from the input image need to be considered: (4,9), (4,10), (4,11), (5,9), (5,10), (5,11), (6,9), (6,10), and (6,11). These nine input pixels are combined into a single binary number that is used as an index in the image enhancement algorithm string.

# For example, to determine the output pixel that corresponds to the very middle pixel of the input image, the nine pixels marked by [...] would need to be considered:

# # . . # .
# #[. . .].
# #[# . .]#
# .[. # .].
# . . # # #
# Starting from the top-left and reading across each row, these pixels are ..., then #.., then .#.; combining these forms ...#...#.. By turning dark pixels (.) into 0 and light pixels (#) into 1, the binary number 000100010 can be formed, which is 34 in decimal.

# The image enhancement algorithm string is exactly 512 characters long, enough to match every possible 9-bit binary number. The first few characters of the string (numbered starting from zero) are as follows:

# 0         10        20        30  34    40        50        60        70
# |         |         |         |   |     |         |         |         |
# ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
# In the middle of this first group of characters, the character at index 34 can be found: #. So, the output pixel in the center of the output image should be #, a light pixel.

# This process can then be repeated to calculate every pixel of the output image.

# Through advances in imaging technology, the images being operated on here are infinite in size. Every pixel of the infinite output image needs to be calculated exactly based on the relevant pixels of the input image. The small input image you have is only a small region of the actual infinite input image; the rest of the input image consists of dark pixels (.). For the purposes of the example, to save on space, only a portion of the infinite-sized input and output images will be shown.

# The starting input image, therefore, looks something like this, with more dark pixels (.) extending forever in every direction not shown here:

# ...............
# ...............
# ...............
# ...............
# ...............
# .....#..#......
# .....#.........
# .....##..#.....
# .......#.......
# .......###.....
# ...............
# ...............
# ...............
# ...............
# ...............
# By applying the image enhancement algorithm to every pixel simultaneously, the following output image can be obtained:

# ...............
# ...............
# ...............
# ...............
# .....##.##.....
# ....#..#.#.....
# ....##.#..#....
# ....####..#....
# .....#..##.....
# ......##..#....
# .......#.#.....
# ...............
# ...............
# ...............
# ...............
# Through further advances in imaging technology, the above output image can also be used as an input image! This allows it to be enhanced a second time:

# ...............
# ...............
# ...............
# ..........#....
# ....#..#.#.....
# ...#.#...###...
# ...#...##.#....
# ...#.....#.#...
# ....#.#####....
# .....#.#####...
# ......##.##....
# .......###.....
# ...............
# ...............
# ...............
# Truly incredible - now the small details are really starting to come through. After enhancing the original input image twice, 35 pixels are lit.

# Start with the original input image and apply the image enhancement algorithm twice, being careful to account for the infinite size of the images. How many pixels are lit in the resulting image?

# Your puzzle answer was 5391.

# --- Part Two ---
# You still can't quite make out the details in the image. Maybe you just didn't enhance it enough.

# If you enhance the starting input image in the above example a total of 50 times, 3351 pixels are lit in the final output image.

# Start again with the original input image and apply the image enhancement algorithm 50 times. How many pixels are lit in the resulting image?

# Your puzzle answer was 16383.

# Both parts of this puzzle are complete! They provide two gold stars: **

# At this point, you should return to your Advent calendar and try another puzzle.

# If you still want to see it, you can get your puzzle input.