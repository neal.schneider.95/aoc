# Advent of code 2021
# Day 17: trick shot https://adventofcode.com/2021/day/17

ymax_target = -57
ymin_target = -90
xmin_target = 240
xmax_target = 292

def shoot(xv, yv):
    x = y = max_hi = 0
    while y >= ymin_target:
        x += xv
        y += yv
        if y > max_hi: 
            max_hi = y
        if xv > 0 : xv -= 1
        yv -= 1
        if xmin_target <= x <= xmax_target and ymax_target <= y <= ymax_target:
        # if x >= xmin_target and x <= xmax_target and y >= ymin_target and y <= ymax_target:
            return max_hi
    return None

tot_max_hi = 0
xs = 0 
ys = 0
count = 0
for yv in range (-90,9):
        for xv in range (240):
            height = shoot(xv,yv)
            if height:
                print ('Hit at ({},{}), height: {}'.format(xv,yv,height))
                count += 1
                if height > tot_max_hi: 
                    tot_max_hi = height
                    xs = xv
                    ys = yv

print ('Max height at ({},{}), height: {}'.format(xs,ys,tot_max_hi))
print ('Count = ', count)

