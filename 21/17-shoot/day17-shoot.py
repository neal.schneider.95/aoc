# Advent of code 2021
# Day 17: trick shot https://adventofcode.com/2021/day/17

ymax_target = -5
ymin_target = -10
xmin_target = 20
xmax_target = 30

def shoot(xv, yv):
    x = y = max_hi = 0
    while y > ymin_target:
        x += xv
        y += yv
        if y > max_hi: max_hi = y
        if xv > 0 : xv -= 1
        yv -= 1
        if x >= xmin_target and x <= xmax_target and y >= ymin_target and y <= ymax_target:
            return max_hi
    return None

tot_max_hi = 0
xs = 0 
ys = 0
for yy in range (0,100000,1000):
    for yz in range (1000):
        for xv in range (3,20):
            yv = yy+yz
            height = shoot(xv,yv)
            if height:
                print ('Hit at ({},{}), height: {}'.format(xv,yv,height))
                if height > tot_max_hi: 
                    tot_max_hi = height
                    xs = xv
                    ys = yv
    print(yy)            

print ('Max height at ({},{}), height: {}'.format(xs,ys,tot_max_hi))

