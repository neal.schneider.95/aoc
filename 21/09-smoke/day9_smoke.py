# Advent of code day 9 solution:  
# https://adventofcode.com/2021/

depths = []
line_num = 0
width = 0
height = 0


def is_min(x:int, y:int) -> int:
    up, left, right, down = False
    if y == 0:
        up = True
    elif y == height: 
        up = depths[x][y] < depths[x][y-1]
    if x == 0:
        left = True
    else: 
        left = depths[x][y] < depths[x][y-1]

     

if __name__ == '__main__':

    with open ('input.txt', 'r') as file:
        for line in file:
            depths.append([])
            for i in range (len(line)-1):
                depths[line_num].append(int(line[i]))
            print (depths[line_num])
            line_num += 1
    width = len (depths[0])
    height = line_num 
    print ('w', width, 'h', height)