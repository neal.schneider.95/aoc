# Advent of code day 15 solution:  
# https://adventofcode.com/2021/

grid = []
steps = [0]
line_count = 10

def read_grid() -> int:
    with open ('sample.txt', 'r') as file:
        for line in file:
            grid.append(list(int(i) for i in line[:-1]))


def print_grid():
    for row in grid:
        for item in row:
            print (item, end='')
        print()

# 
def step_route(row, col):
    print ('Step_route({}, {})'.format(row, col))
    steps[0] += 1
    if steps[0] > 200 : 0/0
    if row == line_count-1 and col == line_count-1:
        return grid[row][col]
    if row == line_count or col == line_count : 
        return 1000000
    down = step_route(row+1, col)
    right = step_route(row, col+1)
    if right < down: 
        print ('Right')
        return right + grid[row][col]
    else: 
        print('down')
        return down + grid[row][col]
    
# Program Start
read_grid()
print ("Input grid:")
print (step_route(0,0))
print ('Done')