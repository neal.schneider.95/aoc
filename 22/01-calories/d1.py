cal_count = 0
cal_list = []
elf_count = 0
line_count = 0
with open('input.txt', 'r') as f:
    for line in f:
        line_count += 1
        if line.strip():
            cal_count += int(line)
        else:
            cal_list.append(cal_count)
            cal_count = 0
            elf_count += 1

cal_list.sort(reverse = True)

print (cal_list[0])
print (sum(cal_list[:3]))
print ('Processed {} elves and {} lines.'.format(elf_count, line_count))