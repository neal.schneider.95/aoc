# TODO: modify to find the spot for the beacon

import re

test_y = 10
test_y = 2000000

filename = 'input.txt'

sb = []         # sample beacon pairs
beacons = set()
sensors = set()
max_dist = 0    # maximum distance between a sensor and it's beacon
with open(filename, 'r') as f:
    for line in f:
        sb.append([])
        s = re.split(r'[x|y]=', line)  
        sb[-1].append(int(s[1].split(',')[0]))
        sb[-1].append(int(s[2].split(':')[0]))
        sb[-1].append(int(s[3].split(',')[0]))
        sb[-1].append(int(s[4]))
        sb[-1].append(abs(sb[-1][0]-sb[-1][2]) + abs(sb[-1][1]-sb[-1][3]))  # add distance as the final item
        if sb[-1][4] > max_dist: max_dist = sb[-1][4]
        sensors.add((sb[-1][0], sb[-1][1]))
        beacons.add((sb[-1][2], sb[-1][3]))
        print(sb[-1])
    print ('Beacons:', beacons)
    print ('Sensors:', sensors)
        
min_x = min(it[0] for it in sb) - max_dist
max_x = max(it[0] for it in sb) + max_dist
min_y = min(it[1] for it in sb) - max_dist
max_y = max(it[1] for it in sb) + max_dist
if min_x < 0: min_x = 0
if min_y < 0: min_x = 0
if max_x > 4000000: max_x = 4000000
if max_y > 4000000: max_x = 4000000

print ('min, max x, dist = ', min_x, max_x, max_dist)
print ('xrange ({}, {}) y range ({}, {})'.format(min_x, max_x, min_y, max_y))

# for i in range(min_x - max_dist, max_x + max_dist):
#     print (i%10,end='')
# print()

# for test_y in range (test_y-5, test_y+5) :
samples = [[0 for i in range(min_x, max_x)] for j in range(min_y, max_y)]
tick = int ((max_y-min_y) / 100) + 1
tick_count = 0

for test_y in range (min_y, max_y) :
    for sensor in sb:
        diff_y = abs(sensor[1] - test_y)
        # print (start, stop)
        if diff_y > sensor[4]: 
            continue
        start = sensor[0] - sensor[4] + diff_y
        stop  = sensor[0] + sensor[4] - diff_y +1
        for i in range(start, stop):
            samples[test_y-min_y][i] = 1
    if test_y % tick == 0:
        print ("calc: {} \r", tick_count, end=0)
        tick_count += 1

    # for i in range(min_x - max_dist, max_x + max_dist):
    #     if (i,test_y) in (sensors | beacons):
    #         if (i,test_y) in sensors:
    #             print ('S', end='')
    #         else:
    #             print ('B', end='')
    #     else:
    #         # print(' ',end='')
    #         print ('#' if samples[i] else '.', end='')

    # print (test_y, sum(samples))


for row, row_samples in enumerate(samples):
    for col, samp in enumerate(row_samples):
        if samp: 
            print("vacancy: (r {}, c {}) TF: {}".format(row+min_y, col+min_y,\
                row+min_y + (col+min_y * 4000000)))
    if row % tick == 0:
        print ("search: {} \r", tick_count, end=0)
        tick_count += 1
    # Part 1:
    # noticed the sample outupt was 1 more than the answer, 
    # so tried 1 less than the ouput for the puzzle input 
    # and got the right answer in the second try

    # 2000000 4582668
    # -1      4582667

