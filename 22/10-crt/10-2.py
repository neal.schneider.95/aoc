cycle = 0
x = 1
line_num = 0
sample_pts = [20, 60, 100, 140, 180, 220]
samples = []
with open('input.txt', 'r') as f:
    for line in f:
        line_num += 1
        cmd = line.split()
        if cmd[0] == 'noop':
            cycle += 1
            if cycle in sample_pts:
                samples.append(x)
                # print('{}:{} x = {}'.format(line_num, line, x))
            if cycle % 40 - 1 <= x <= cycle % 40 + 1:
                print ('#', end='')
            else:
                print (' ', end='')
            if cycle % 40 == 0:
                print()
        elif cmd[0] == 'addx':
            cycle += 1
            if cycle in sample_pts:
                samples.append(x)
                # print('{}:{} x = {}'.format(line_num, line, x))
            if cycle % 40 - 1 <= x <= cycle % 40 + 1:
                print ('#', end='')
            else:
                print (' ', end='')
            if cycle % 40 == 0:
                print()
            cycle += 1
            x += int(cmd[1])
            if cycle in sample_pts:
                samples.append(x - int(cmd[1]))
                # print('{}:{} x = {}'.format(line_num, line, x))
            if cycle % 40 - 1 <= x <= cycle % 40 + 1:
                print ('#', end='')
            else:
                print (' ', end='')
            if cycle % 40 == 0:
                print()
        else:
            print('unknown command:', cmd[0])
            exit()
print ('cycle', cycle)
print (samples)
signal_strengths = [sample_pts[i] * samples[i] for i in range(len(sample_pts))]
print (signal_strengths)
print (sum(signal_strengths))
        
# output: BRJLFULP

##  ###    ## #    #### #  # #    ###  #
  # #  #    # #    #    #  # #    #  # #
##  #  #    # #    ###  #  # #    #  # #
  # ###     # #    #    #  # #    ###  #
  # # #  #  # #    #    #  # #    #    #
##  #  #  ##  #### #     ##  #### #