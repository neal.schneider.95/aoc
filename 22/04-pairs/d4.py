def printrange (e: list):
    print(' '* e[0]+ '*'*e[1])

contain = 0
overlap = 0
with open('.txt', 'r') as f:
    for line in f:
        d = line.strip().split(',')
        e1 = [int(e) for e in d[0].split('-')]
        e2 = [int(e) for e in d[1].split('-')]

        # printrange(e1)
        # printrange(e2)
        if not (e1[1] < e2[0] or e1[0] > e2[1]):
            overlap += 1
            print ('o', end='')
        else:
            print (' ', end='')
        
        if e1[0] <= e2[0] and e1[1] >= e2[1] or\
           e1[0] >= e2[0] and e1[1] <= e2[1]:
           contain += 1
           print ('c', end='')
        else: print (' ', end='')
        
        print ('   ', e1, e2)

print (contain)
print (overlap)
# try 1= 529