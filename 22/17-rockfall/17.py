import copy
rocks =[[0b0011110],

        [0b0001000,
         0b0011100,
         0b0001000],

        [0b0000100,
         0b0000100,
         0b0011100],

        [0b0010000,
         0b0010000,
         0b0010000,
         0b0010000],

        [0b0011000,
         0b0011000]]

def hit_check(cave, rock):
    for i in len(rock):
        if cave[i] + rock[i] != cave[i] & rock[i]: 
            return True
    return False

# mask for the width of the rocks
rock_mask = [0b0011110, 0b0011100, 0b0010000, 0b0011000]
# height of the rocks, will be needed several times and rather than do len(rock)..
rock_ht = [3,3,4,2]

with open ('sample.txt', 'r') as f:
    jets = f.read()
njets = len(jets)

cave = [0x7F, 0]
pile = 0        # Height of the pile
step = 0        # number of steps fallen and jets puffed.
rock_stopped = 0
pop_rock = True
while step < 2023:
    while cave[pile] != 0: pile -= 1            # find the top of the pile
    if pile < len(cave) + 10: cave += [0]*10    # add a bit to the cave height

    if pop_rock:                                # Start a new rock falling
        rock = copy.copy(rock[step % len(rocks)])         # set rock 
        rm = rock_mask[step % len(rocks)]       # set rock mask
        rh = rock_ht[step % len(rocks)]         #
        pile = len(cave) - 1
        pop_rock = False
    
    # Move rock by jets
    if jets[step % njets] == '<':                 # shift rock and mask left
        if not (rm & 0b1000000):
            rm <<= 1
            trock = copy.copy(rock)
            for i in range(rh):
                trock[i] <<= 1
    else:                                         # shift right
        if not (rm & 1):
            rm >>= 1
            trock = copy.copy(rock)
            for i in range(rh):
                trock[i] >>= 1 
    step += 1
    if hit_check(cave, trock):
        