with open ('sample.txt', 'r') as f:
    data = [line.strip() for line in f]
size = len(data[0])
# print ('size = {}'.format(size))

high_score = (0,)
for row, line in enumerate(data):
    for col, tree_height_str in enumerate(line):
        if row == 0 or row == size-1 or col == 0 or col == size-1:
            continue
        tree_height = int(tree_height_str)
        vl = vr = vu = vd = 0   # views to the left, right, up and down
        rc = row - 1            # row check index
        while rc >= 0:
            if int(data[rc][col]) <= tree_height : 
                vu = row - rc
                break
            rc -= 1
        rc = row + 1
        while rc < size:
            if int(data[rc][col]) <= tree_height: 
                vd = rc - row
                break
            rc += 1
        cc = col - 1                # col check index
        while cc >= 0:
            if int(data[row][cc]) <= tree_height: 
                vl = col - cc
                break
            cc -= 1
        cc = col + 1
        while cc < size:
            if int(data[row][cc]) <= tree_height: 
                vr = cc - col
                break
            cc += 1
        score = vl * vr * vu * vd
        if score > high_score[0]:
            high_score = (score, (row, col), (vl, vr, vu, vd))
    # print()
print ("Higest Senic Score:", high_score)