with open ('input.txt', 'r') as f:
    data = [line.strip() for line in f]
size = len(data[0])
# print ('size = {}'.format(size))

high_score = (0,)
for row, line in enumerate(data):
    for col, tree_height_str in enumerate(line):
        if row == 0 or row == size-1 or col == 0 or col == size-1:
            score = 0
        else:
            tree_height = int(tree_height_str)
            vl = vr = vu = vd = 1
            for rc in range (row-1,0,-1):
                if int(data[rc][col]) >= tree_height: 
                    vu = row - rc+1
                    break
            for rc in range (row+1, size):
                if int(data[rc][col]) >= tree_height: 
                    vd = rc - row+1
                    break
            for cc in range (col-1, 0, -1):
                if int(data[row][cc]) >= tree_height: 
                    vl = col - cc+1
                    break
            for cc in range (col+1, size):
                if int(data[row][cc]) >= tree_height: 
                    vr = cc - col +1
                    break
            score = vl * vr * vu * vd
            rc = (row, col)
            if score > high_score[0]:
                high_score = (score, rc, (vl, vr, vu, vd))
            print ('tree {} ({}) {}: {} {} {} {}) '.format(rc, data[row][col], score, vu, vd, vl, vr))
        # print('{}'.format(score), end=' ')
    print()
print("Higest Senic Score:", high_score)