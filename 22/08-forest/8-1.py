with open ('input.txt', 'r') as f:
    data = [line.strip() for line in f]
size = len(data[0])
print ('size = {}'.format(size))

count = 0
visible_trees = []

for row, line in enumerate(data):
    for col, tree_height in enumerate(line):
        if row == 0 or row == size-1 or col == 0 or col == size-1:
            count += 1
            # visible_trees.append((row,col))
            continue
        vl = vr = vu = vd = True
        for rc in range (0,row):
            if data[rc][col] >= tree_height: 
                vu = False
                # print ('tree {} ({}) blocked U by {} ({}) '.format(
                #     (row, col), data[row][col], (rc, col), data[rc][col]))
                break
        for rc in range (row+1, size):
            if data[rc][col] >= tree_height: 
                vd = False
                # print ('tree {} ({}) blocked D by {} ({}) '.format(
                #     (row, col), data[row][col], (rc, col), data[rc][col]))
                break
        for cc in range (0, col):
            if data[row][cc] >= tree_height: 
                vl = False
                # print ('tree {} ({}) blocked U by {} ({}) '.format(
                #     (row, col), data[row][col], (row, cc), data[row][cc]))
                break
        for cc in range (col+1, size):
            if data[row][cc] >= tree_height: 
                vr = False
                # print ('tree {} ({}) blocked D by {} ({}) '.format(
                #     (row, col), data[row][col], (row, cc), data[row][cc]))
                continue
        if vl or vr or vu or vd:
            visible_trees.append((row,col,tree_height))
            count += 1
    # print()
print (visible_trees)
print ("Visible trees =", count)