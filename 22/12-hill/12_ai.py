def find_fewest_steps(heightmap, start, goal):
# create the queue and add the starting position and 0 steps
    queue = [(start, 0)]

    # while the queue is not empty
    while queue:
        # pop the first position and number of steps from the queue
        pos, steps = queue.pop(0)
        
        # check if the position is the goal
        if pos == goal:
            return steps
        
        # iterate through the four possible moves
        for move in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
            # calculate the new position
            new_pos = (pos[0] + move[0], pos[1] + move[1])
            
            # check if the new position is within the bounds of the heightmap
            # and the elevation difference is at most 1
            if 0 <= new_pos[0] < len(heightmap) and 0 <= new_pos[1] < len(heightmap[0]):
                elev_diff = ord(heightmap[new_pos[0]][new_pos[1]]) - ord(heightmap[pos[0]][pos[1]])
                if elev_diff >= 0 and elev_diff <= 1:
                    # add the new position and number of steps to the queue
                    queue.append((new_pos, steps + 1))

    # if the queue is empty, return -1
    return -1


heightmap = [
"Sabqponm",
"abcryxxl",
"accszExk",
"acctuvwj",
"abdefghi"
]
start = (0, 0)
goal = (2, 5)
print(find_fewest_steps(heightmap, start, goal)) # should print 31