
row = col = 0
data = []

# input data into 2-d array
with open('sample.txt', 'r') as f:
    row= 0
    for line in f:
        data.append([])
        for col, c in enumerate(line.strip()):
            if 'a' <= c <= 'z':
                data[row].append(ord(c)-ord('a'))
            else:
                if c == 'S':
                    data[row].append(0)
                    start = (row, col)
                elif c == 'E':
                    data[row].append(26)
                    end = (row, col)
                else:
                    print ('Invalid char {} at ({}, {})'.format(c,row,col))
                    exit()
            # print ('({},{}):{}{}'.format(row, col, c, data[row][col]), end='')
        row += 1
        # print()
num_rows = row
num_cols = col + 1
print ('map size = {}rows, {}cols'.format(row, col))

# print array to validate
for row in range(num_rows):
    for col in range(num_cols):
        print ('{:>3}'.format(data[row][col]), end='')
    print()
print ('start = {}, end = {}'.format(start, end))

# create map m of possible movements:
# assumes no negative moves
m = dict()
for row in range(num_rows):
    for col in range(num_cols):
        m[(row,col)] = set()
        vals = (data[row][col], data[row][col]+1)
        if row > 0 and data[row-1][col] in vals:
            m[(row,col)].add((row-1,col),)
        if row < num_rows-1 and data[row+1][col] in vals:
            m[(row,col)].add((row+1,col),)
        if col > 0 and data[row][col-1] in vals: 
            m[(row,col)].add((row,col-1),)
        if col < num_cols-1 and data[row][col+1] in vals: 
            m[(row,col)].add((row,col+1),)

# print map for validity check
for item in m.keys():
    print ('{}->{}'.format(item, m[item]))

# Whew... now implement Djikstra's


# from collections import defaultdict
# import heapq as heap

# def dijkstra(G, startingNode, end):
# 	visited = set()
# 	parentsMap = {}
# 	pq = []
# 	nodeCosts = defaultdict(lambda: float('inf'))
# 	nodeCosts[startingNode] = 0
# 	heap.heappush(pq, (0, startingNode))
 
# 	while pq:
# 		# go greedily by always extending the shorter cost nodes first
# 		_, node = heap.heappop(pq)
# 		visited.add(node)
 
# 		for adjNode, weight in G[node].items():
# 			if adjNode in visited: continue
# 			newCost = nodeCosts[node] + weight
#             # if adjNode == end:
#             #     return parentsMap, nodeCosts
# 			if nodeCosts[adjNode] > newCost:
# 				parentsMap[adjNode] = node
# 				nodeCosts[adjNode] = newCost
# 				heap.heappush(pq, (newCost, adjNode))
        
# 	return parentsMap, nodeCosts

# path, costs = dijkstra(m, start, end)
# print ('paths = ', path)

print (m)

visited = node_list = [start]
i = 0
# while i < len (m):