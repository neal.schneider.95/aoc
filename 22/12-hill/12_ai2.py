def find_shortest_path(heightmap, start, end):
  # Create a queue to store the positions we need to visit
  queue = []
  # Add the starting position to the queue
  queue.append(start)
  # Create an array to store the number of steps it took to reach each position
  steps = []
  # Initialize the steps array with -1 for every position
  for i in range(len(heightmap)):
    row = []
    for j in range(len(heightmap[i])):
      row.append(-1)
    steps.append(row)
  # Set the number of steps at the starting position to 0
  steps[start[0]][start[1]] = 0
  # While there are still positions in the queue
  while len(queue) > 0:
    # Dequeue the first position
    pos = queue.pop(0)
    # If we have reached the ending position, return the number of steps it took
    if pos == end:
      return steps[pos[0]][pos[1]]
    # Check the neighbors of the current position
    for dx in [-1, 0, 1]:
      for dy in [-1, 0, 1]:
        # Skip the current position
        if dx == 0 and dy == 0:
          continue
        # Calculate the position of the neighbor
        x = pos[0] + dx
        y = pos[1] + dy
        # Skip the neighbor if it is out of bounds
        if x < 0 or y < 0 or x >= len(heightmap) or y >= len(heightmap[0]):
          continue
        # Skip the neighbor if it has an elevation more than one level higher than the current position
        if ord(heightmap[x][y]) > ord(heightmap[pos[0]][pos[1]]) + 1:
          continue
        # If the number of steps to reach the neighbor has not been set, set it to one more than the number of steps to reach the current position
        if steps[x][y] == -1:
          steps[x][y] = steps[pos[0]][pos[1]] + 1
          # Add the neighbor to the queue
          queue.append((x, y))
        print (queue)
  # If we reach this point, it means we were unable to find a path to the ending position
  return -1


heightmap = [
"Sabqponm",
"abcryxxl",
"accszExk",
"acctuvwj",
"abdefghi"
]
start = (0, 0)
goal = (2, 5)
print(find_shortest_path(heightmap, start, goal)) # should print 31