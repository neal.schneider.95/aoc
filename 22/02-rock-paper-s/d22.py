# https://adventofcode.com/2022/day/2

score = w = l = t = lc = 0

with open('input.txt', 'r') as f:
    for line in f:
        op = ord(line[0]) - ord('A') + 1
        outcome = ord(line[2]) - ord('X') + 1
        # print (op, me, end=' ')
        if outcome == 1:                            # lose
            if op == 1:
                score += 3 # scissors looses to rock
            elif op == 2:
                score += 1 # rock looses to paper 
            else:
                score += 2# paper looses to scissors
            l += 1
            # print ('t {} {}'.format (me, 3 + me))
        elif outcome == 2:                           # draw
            score += op + 3
            t += 1
            # print ('w {} {}'.format (me, 0 + me))
        else:                                         # win
            if op == 1:
                score +=  8  #6 + 2;
            elif op == 2:
                score += 9 
            else:
                score += 7
            w += 1
            # print ('l {} {}'.format (me, 6 + me))
print (w, l, t, score) 

# Got it the first time!!! 
# PS C:\code\aoc\22\02-rock-paper-s> python3 .\d22.py
# 567 559 1374 11696