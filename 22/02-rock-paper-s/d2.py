score = w = l = t = lc = 0

with open('input.txt', 'r') as f:
    for line in f:
        op = ord(line[0]) - ord('A') + 1
        me = ord(line[2]) - ord('X') + 1
        print (op, me, end=' ')
        if op == me:                            # tie
            score += 3 + me
            t += 1
            print ('t {} {}'.format (me, 3 + me))
        elif op - me == 1 or op - me == -2:     # loss
            score += me
            l += 1
            print ('w {} {}'.format (me, 0 + me))
        else:                                   # win
            score += 6 + me
            w += 1
            print ('l {} {}'.format (me, 6 + me))
print (w, l, t, score) 
# try 1: 1502 927 71 6733

# try 2: Got it!  1539 596 365 15337


