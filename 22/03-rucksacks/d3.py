# https://adventofcode.com/2022/day/3

def priority (c:str) -> int:
    pri = ord(common.lower()) - ord('a') + 1
    if common.isupper():
        pri += 26
    return pri

group_sum = 0
pri_sum = 0
line_count = 0
group = []
with open('input.txt', 'r') as f:
    for line in f:
        group.append(line)
        line = line.strip()
        m = len (line) // 2 
        r1 = line[:m]
        r2 = line[m:]
        common = ''
        for c in r1:
            if c in r2:
                common = c
                break
        pri = priority(common)
        print ("{} = {} . {}".format(line, r1, r2))
        print (common, pri)
        pri_sum += pri 

        # process groups:
        if line_count % 3 == 2:
            for c in group[0]:
                if c in group[1] and c in group[2]:
                    common = c
                    break
            group_sum += priority(common)
            print ("Group --->", common, priority(common))
            group = []
        line_count += 1

print (pri_sum)
print (group_sum)