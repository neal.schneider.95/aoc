line_num = 0
with open ('input.txt', 'r') as f:
    for line in f:
        index = 4
        # print (line.strip())
        chars = line[:14]
        while True:
            # print (' ' * (index - 4), chars)
            chars = chars[1:] + line[index]
            double = False
            index += 1
            for i in range (14):
                if chars.count(chars[i]) > 1: 
                    double = True
            if not double:
                break
        line_num += 1
        print (line_num, index,chars)
