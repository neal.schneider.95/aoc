line_num = 0
with open ('sample.txt', 'r') as f:
    for line in f:
        index = 4
        print (line.strip())
        chars = [line[i] for i in range(4)]
        while not line[index] in chars:
            print (' ' * (index - 4), ''.join(chars))
            chars.append(line[index])
            chars.pop(0)
            index += 1
        line_num += 1
        print (line_num, index, ''.join(chars))
