def printstacks(s):
    for stack in range(len(s)):
        print(s[stack])


# sample sd = 3, input sd = 8
stack_depth = 8
stack_data = []

with open ('input.txt', 'r') as f:
    # read in stack data
    for i in range (stack_depth):
        stack_data.append(f.readline())
    line = f.readline()
    nstacks = int(line.split()[-1])
    

    # parse stack data
    stacks = [[] for i in range(nstacks)]
    while stack_data:
        line = stack_data.pop()
        for s in range(nstacks):
            c = line[s*4+1]
            if c.isalpha():
                stacks[s].append(c)

    print ('BEFORE::::')
    printstacks(stacks)

    # process movements [for CrateMover 9001!]
    f.readline()
    line = f.readline()
    while (line):
        # print(line)
        tok = line.split()
        n = int(tok[1])
        fs = int(tok[3])-1
        ts = int(tok[5])-1

        temp_stack = []
        for i in range(n):
            temp_stack.append(stacks[fs].pop())

        for i in range(n):
            stacks[ts].append(temp_stack.pop())

        # printstacks(stacks)
        line = f.readline()

    print ('AFTER::::')
    printstacks(stacks)

for i in range(nstacks):
    print(stacks[i][-1], end='')