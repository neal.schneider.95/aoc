dec_dot = "1234567890."
def ispart (data, row, start, end):
    print(f"ispart({data[row][start:end]})")
    if start > 0 and data[row][start-1] not in dec_dot:
        print('l', data[row][start-1])
        return True
    if end < len(data[row])-1 and data[row][end+1] not in dec_dot:
        print('r', data[row][end+1])
        return True
    if row > 0:
        for i in range(max(0, start-1), min(len(data[row]), end+1)):
            if data[row-1][i] not in dec_dot:
                print('u', data[row-1][i])
                return True
    if row < len(data):
        for i in range(max(0, start-1), min(len(data[row]), end+1)):
            if data[row+1][i] not in dec_dot:
                print('d', data[row+1][i])
                return True
    return False

with open ("sample.txt", 'r') as f:
    data = f.readlines()
    # nlines = len(data)
    width = len(data[0])
    sum = 0
    for row, line in enumerate(data):
        col = 0
        while col < width-1:
            if line[col].isdecimal():
                start = col
                while line[col].isdecimal():
                    col += 1
                if ispart(data, row, start, col):
                    part = int(data[row][start:col])
                    print(part)
                    sum += part
            col += 1
print ("Total:", sum)
