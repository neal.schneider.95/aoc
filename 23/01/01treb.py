with open('input.txt', "r") as f:
    sum = 0
    for f_line in f:
        line = f_line.strip()
        print(line)
        first = -1
        for char in line:
            if char >= '0' and char <= '9':
                first = int(char)
                break
        second = -1
        for i in range(len(line)-1, -1, -1):
            char = line[i]
            if char >= '0' and char <= '9':
                second = int(char)
                break
        if first == -1 or second == -1:
            print (f'No int chars in line: {line} ({first}, {second})')
            exit(1)
        lineval = first * 10 + second
        print(lineval)
        sum += lineval
print(f"Sum = {sum}")
