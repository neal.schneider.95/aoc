with open('sample.txt', "r") as f:
    sum = 0
    for f_line in f:
        line = list(f_line.strip())
        print(line, end=', ')
        while not line[0].isdigit:
            line.pop(0)
        while not line[-1].isdigit:
            line.pop()
        print(line, end=", ")
        lineval = int(line[0]) * 10 + int(line[-1])
        print(lineval)
        sum += lineval
print(f"Sum = {sum}")
    
