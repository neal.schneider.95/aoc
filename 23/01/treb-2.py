nlist = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
dlist = ['z0o', 'o1e', 't2o', 't3e', 'f4r', 'f5e', 's6x', 's7n', 'e8t', 'n9e']
with open('input.txt', "r") as f:
    sum = 0
    for o_line in f:
        o_line = line = o_line.strip()
        for i in range(len(line)-1):
            for j in range(0,10):
                if line[i:i+len(nlist[j])] == nlist[j]:
                    line = line.replace(nlist[j], dlist[j], 1)
        first = -1
        for char in line:
            if char >= '0' and char <= '9':
                first = int(char)
                break
        second = -1
        for i in range(len(line)-1, -1, -1):
            char = line[i]
            if char >= '0' and char <= '9':
                second = int(char)
                break
        if first == -1 or second == -1:
            print (f'No int chars in line: {line} ({first}, {second})')
            exit(1)
        lineval = first * 10 + second
        print(f"{o_line}, {line}, {lineval}")
        sum += lineval
print(f"Sum = {sum}")

# Frst try for part 2: 54905 Too High
# Another try was 54889
# 54845 Ans for #2