sum = 0
with open("input.txt",'r') as f:
    for line in f:
        bag = {}
        game_data = line.split(':')
        game_number = int(game_data[0].split(' ')[1])
        pulls = game_data[1].split(';')
        possible = True
        for pull in pulls:
            for dice in pull.strip().split(','):
                count = int(dice.strip().split()[0])
                color = dice.strip().split()[1]
                if color in bag.keys():
                    if count > bag[color]: 
                        bag[color] = count
                else:
                    bag[color]= count
        power = 1
        for item in bag.items():
            power *= item[1]
        print (bag, power)
        sum += power
print(sum)