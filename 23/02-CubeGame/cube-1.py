bag = {'red':12, 'green': 13, 'blue': 14}
sum = 0
with open("input.txt",'r') as f:
    for line in f:
        game_data = line.split(':')
        game_number = int(game_data[0].split(' ')[1])
        pulls = game_data[1].split(';')
        possible = True
        for pull in pulls:
            for dice in pull.strip().split(','):
                count = int(dice.strip().split()[0])
                color = dice.strip().split()[1]
                if count > bag[color]:
                    possible = False
                    break
        if possible:
            sum += game_number
print(sum)